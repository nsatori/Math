from bs4 import BeautifulSoup

# 读取本地HTML文件
file_path = r'C:\Users\sbsda\Desktop\213.html'
with open(file_path, 'r', encoding='utf-8') as file:
    html_content = file.read()

# 使用Beautiful Soup解析HTML
soup = BeautifulSoup(html_content, 'html.parser')


trader_id=soup.find('span', class_='max-w-[86%] font-[700] text-[32px] <md:(text-[24px]) text-content-primary').text.strip()
print(f"交易员:{trader_id}")

# 查找所有产品条目的父级div
product_entries = soup.select('div.text-[12px].text-content-tertiary.py-[15px].border-0.border-b-[1px].border-solid.border-border-strength-100.border-t-[1px], \
                              div.text-[12px].text-content-tertiary.py-[15px].border-0.border-b-[1px].border-solid.border-border-strength-100')
# 创建字典来存储分类后的信息
product_info = {}

# 遍历每个产品条目
for index, entry in enumerate(product_entries, start=1):
    # 查找币种
    trader_name = entry.find('span', class_='text-content-primary').text.strip()
    
    # 查找空单或多单信息
    position_type = entry.find('span', class_='text-content-always-white').text.strip()
    
    # 查找杠杆信息
    leverage = entry.find('span', class_='bg-border-strength-100').text.strip()
    
    # 查找订单入场价和开单时间
    entry_price_span = entry.find('span', string='订单入场价')
    if entry_price_span:
        entry_price = entry.find('span', string='订单入场价').find_previous().text.strip()
    
    order_time_span = entry.find('span', string='订单入场价')
    if order_time_span:
        order_time = entry.find('span', string='订单入场价').find_next('span').text.strip()
    
    average_price_span = entry.find('span', string='当前价')
    if average_price_span:
        average_price = entry.find('span', string='当前价').find_next('span').find_next('span').find_next('span').text.strip()
    
    # 判断字典中是否已存在该币种和空单多单类型的条目
    key = (trader_name, position_type)
    if key not in product_info:
        product_info[key] = {
            '币种': trader_name,
            '空单或多单': position_type,
            '信息': []
        }
    
    # 构建每条信息的字典
    info_dict = {
        '杠杆': leverage,
        '入场价': entry_price,
        '开单时间': order_time,
        '持仓均价': average_price
    }
    
    # 将信息添加到对应的产品类别中
    product_info[key]['信息'].append(info_dict)

# 输出每类产品的信息
for index, info in enumerate(product_info.values(), start=1):
    print(f"产品类别 {index}:")
    print(f"币种: {info['币种']}")
    print(f"空单或多单: {info['空单或多单']}")
    print("详细信息:")
    for entry_info in info['信息']:
        print(f"    杠杆: {entry_info['杠杆']}, 入场价: {entry_info['入场价']}, 开单时间: {entry_info['开单时间']}, 持仓均价: {entry_info['持仓均价']}")
    print("-----------------------------")
