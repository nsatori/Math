import pylab as plt
import numpy as np
#精度为e-5
def fx(x,a):
    return (x**2+a)/(x+a)

a=np.arange(0.1,1,1/10000,dtype=float)
x=np.arange(0,1,1/10000,dtype=float)
#储存每次循环的数据
x2=[]
y2=[]
a2=[]
#每次循环调用min取最小值
for i in a:
    ffx=fx(x,i)
    ffx=ffx.tolist()
    x2.append(x[ffx.index(min(ffx))])
    y2.append(min(ffx))
    a2.append(i)

plt.figure()
plt.plot(a2,y2)
plt.show()
plt.figure()
plt.plot(a2,x2)
plt.show()


