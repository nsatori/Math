import pylab as plt
import numpy as np
from scipy.optimize import curve_fit
import pandas as pd
#读取数据
data=pd.read_excel(r"https://gitee.com/nsatori/Math/raw/master/i.xlsx",index_col=0,header=None)
x=data.loc['x']
y=data.loc['y']
x=x.values.astype(float)
y=y.values.astype(float)
#拟合、预测
def fx(x,a,b,c):
    return a*x*x+b*x+c
p1=curve_fit(fx,x,y)[0]
print("a=",p1[0],",b=",p1[1],",c=",p1[2])
ffx=fx(x,*p1)
#制图
plt.figure()
plt.plot(x,y,'.',x,ffx)
plt.show()