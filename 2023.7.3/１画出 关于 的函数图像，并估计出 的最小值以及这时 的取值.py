import pylab as plt
import numpy as np
#精度为e-6
def fx(x,a):
    return (x**2+a)/(x+a)
x=np.arange(0,1,1/1000000,dtype=float)
ffx=fx(x,0.5)
plt.figure()
plt.plot(x,ffx)
plt.show()
#转序列快速找最小值
ffx=ffx.tolist()
x=x.tolist()
print("当x为",x[ffx.index(min(ffx))],"时，y取到最小值",min(ffx))